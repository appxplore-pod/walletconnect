// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.6 (swiftlang-5.6.0.323.62 clang-1316.0.20.8)
// swift-module-flags: -target arm64-apple-ios13.4 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name WalletConnectLib
import CoreImage.CIFilterBuiltins
import CoreImage
import CryptoSwift
import Foundation
import Swift
import UIKit
import WalletConnectSwift
import _Concurrency
@objc public enum WCStatusCode : Swift.Int {
  case success = 0, already_loggedIn = 1, not_loggedIn = 2, wallet_error = 3, player_cancel = 4, wrong_chain_id = 5
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public protocol WalletConnectDelegate {
  @objc func onPersonalSigned(code: WalletConnectLib.WCStatusCode, data: Swift.String)
  @objc func onETHSigned(code: WalletConnectLib.WCStatusCode, data: Swift.String)
  @objc func onTransactionSent(code: WalletConnectLib.WCStatusCode, data: Swift.String)
  @objc func onConnectCallback(code: WalletConnectLib.WCStatusCode, data: Swift.String)
  @objc func onDisconnected(code: WalletConnectLib.WCStatusCode)
}
@objc public class WCLib : ObjectiveC.NSObject {
  @objc public init(appView: UIKit.UIViewController, appName: Swift.String, appDesc: Swift.String, appUrl: Swift.String, appVersion: Swift.Int, appIcons: [Swift.String], bridgeUrl: Swift.String, chainId: Swift.Int, delegate: WalletConnectLib.WalletConnectDelegate, isDebug: Swift.Bool = false)
  @objc public func isWalletConnected() -> Swift.Bool
  @objc public func accountAddress() -> Swift.String
  @objc public func connect()
  @objc public func disconnect()
  @objc public func personal_sign(message: Swift.String)
  @objc public func eth_sign(message: Swift.String)
  @objc public func send_transaction(toAddress: Swift.String, data: Swift.String, value: Swift.String?, gasLimit: Swift.String)
  @objc public func keccak(byteData: Swift.Array<Swift.UInt8>) -> Swift.String
  @objc public func keccak(message: Swift.String) -> Swift.String
  @objc deinit
}
extension WalletConnectLib.WCLib : WalletConnectSwift.ClientDelegate {
  public func client(_ client: WalletConnectSwift.Client, didFailToConnect url: WalletConnectSwift.WCURL)
  public func client(_ client: WalletConnectSwift.Client, didConnect url: WalletConnectSwift.WCURL)
  public func client(_ client: WalletConnectSwift.Client, didConnect session: WalletConnectSwift.Session)
  public func client(_ client: WalletConnectSwift.Client, didDisconnect session: WalletConnectSwift.Session)
  public func client(_ client: WalletConnectSwift.Client, didUpdate session: WalletConnectSwift.Session)
}
extension WalletConnectLib.WCStatusCode : Swift.Equatable {}
extension WalletConnectLib.WCStatusCode : Swift.Hashable {}
extension WalletConnectLib.WCStatusCode : Swift.RawRepresentable {}
